# kok-aws-rnd

kok rnd repo

# gitlab ci/cd -> ssh ec2 bash git pull origin master
- status = <b>done</b>
- folder = gitlab -> ec2

# gitlab ci/cd -> aws CF -> deploy lambda w/o VPC
- status = <b>done</b>
- folder = gitlab -> aws CF / lambda / without vpc

# gitlab ci/cd -> aws CF -> deploy lambda w/ VPC
- status = <b>done</b>
- folder = gitlab -> aws CF / lambda / with vpc

# gitlab ci/cd -> aws CF -> create role
- status = <b>done</b>
- folder = gitlab -> aws CF / iam / role

# gitlab ci/cd -> aws CF -> attach policy
- status = <b>done</b>
- folder = gitlab -> aws CF / iam / policy

# gitlab ci/cd -> aws CF -> aws EB rules
- status = <b>done</b>
- folder = gitlab -> aws CF / EB / rules

# gitlab ci/cd -> aws CF -> aws EB bus
- status = <b>Not For Now</b>
- folder = gitlab -> aws CF / EB / bus

# gitlab ci/cd -> aws CF -> aws CW
- status = <b> 0% </b>
- folder = --

# gitlab ci/cd -> aws CF -> docDB
- status = <b> 0% </b>
- folder = gitlab -> aws CF / docDB

# gitlab ci/cd -> aws CF -> aws glue job
- status = <b> pending review </b>
- folder = gitlab -> aws CF / glue / job

# gitlab ci/cd -> aws CF -> amplify w/o branch
- status = <b>done</b>
- folder = gitlab -> aws CF / amplify / without branch attached

# gitlab ci/cd -> aws CF -> amplify w/ branch
- status = <b>done</b>
- folder = gitlab -> aws CF / amplify / with branch attached

# gitlab ci/cd -> aws CF -> cognito identity pool
- status = <b> 0% </b>
- folder = gitlab -> aws CF / cognito / identity pool

# gitlab ci/cd -> aws CF -> cognito user pool
- status = <b> 0% </b>
- folder = gitlab -> aws CF / cognito / user pool
