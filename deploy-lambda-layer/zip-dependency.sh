set -euo pipefail
IFS=$'\n\t'

while getopts p: args; do
  case "${args}" in
    p) path=${OPTARG};;
  esac
done

for whl in $path; do
  pip3 install -t python/ $whl
  cd python/
  rm -r -f *.dist-info __pycache__
  cd ..
  zip -r "${whl##*/}".zip python/
  aws s3 cp "${whl##*/}".zip s3://kok-rnd-jp-bucket/lambda-layer/
  rm -r python/
done
