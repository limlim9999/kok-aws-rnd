set -euo pipefail
IFS=$'\n\t'

for i in ./lambda/script/*; do
  zip -j -r $i.zip $i
  aws s3 cp $i.zip s3://kok-rnd-jp-bucket/lambda-script/
done
