#!/bin/bash

# get server list, i duno what is this.... code copied from https://www.youtube.com/watch?v=knRKNfQlQ-E
set -f
string=$EC2_SERVER_IP
array=(${string//,/ })

# iterate servers for deploy and pull last commit
for i in "${!array[@]}"; do
    echo "Deploy project on server ${array[i]}"
    ssh ec2-user@${array[i]} "cd gitlab-deploy-lambda-test && git pull origin master"
done 
